package Krzysztof.Geisler;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        CarMarket giełda = new CarMarket();

        giełda.dodajOgłoszenie(1, "Volkswagen", "Golf", "Czarny", "04.01.1993", 2000);
        giełda.dodajOgłoszenie(1, "Seat", "Ibiza", "Czarny", "04.01.1993", 4000);
        giełda.dodajOgłoszenie(1, "Audi", "A3", "Czarny", "04.01.1993", 5000);
        giełda.wyświetlListęSamochodówOsobowych();

        giełda.usuńOgłoszenie(1, 2);
        giełda.wyświetlListęSamochodówOsobowych();

    }




}


