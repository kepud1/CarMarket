package Krzysztof.Geisler;

/**
 * Created by krzys on 01.08.2017.
 */
public class SamochodyDostawcze {
    String marka;
    String model;
    String kolor;
    String dataProdukcji;
    int cena;
    int id;
    static int zmienna = 1;

    public SamochodyDostawcze(Builder builder) {
        this.marka = builder.marka;
        this.model = builder.model;
        this.kolor = builder.kolor;
        this.dataProdukcji = builder.dataProdukcji;
        this.cena = builder.cena;
        this.id=builder.id;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public String getDataProdukcji() {
        return dataProdukcji;
    }

    public void setDataProdukcji(String dataProdukcji) {
        this.dataProdukcji = dataProdukcji;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id =id;
    }

    public static class Builder {
        String marka;
        String model;
        String kolor;
        String dataProdukcji;
        int cena;
        int id;
        static int zmienna = 1;

        public Builder ustawMarkę (String marka) {
            this.marka = marka;
            return this;
        }

        public Builder ustawModel(String model) {
            this.model = model;
            return this;
        }

        public Builder ustawKolor(String kolor) {
            this.kolor = kolor;
            return this;
        }

        public Builder ustawDatęProdukcji(String dataProdukcji) {
            this.dataProdukcji = dataProdukcji;
            return this;
        }

        public Builder ustawCenę(int cena) {
            this.cena = cena;
            return this;
        }

        public Builder ustawId() {
            this.id = zmienna++;
            return this;
        }

        public SamochodyDostawcze build() {
            return new SamochodyDostawcze(this);
        }




    }
}
